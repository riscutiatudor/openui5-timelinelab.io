## Introduction
A timeline control for openUI5 based on https://www.w3schools.com/howto/howto_css_timeline.asp

## Demo
A short demo can be found here https://riscutiatudor.gitlab.io/openui5-timeline.gitlab.io/

