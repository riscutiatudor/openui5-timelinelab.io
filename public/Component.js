sap.ui.define([
	"sap/ui/core/UIComponent"
], function(UIComponent) {
	"use strict";

	/**
	 * Component
	 *
	 * @author Tudor Riscutia
	 * @version 1.0
	 */
	return UIComponent.extend("ro.riscutiatudor.Component", {

		metadata : {
			manifest : "json"
		},

		/**
		 * Initialize component
		 * 
		 * @public
		 * @override
		 */
		init : function() {
			UIComponent.prototype.init.apply(this, arguments);
		},

		/**
		 * Destroy component
		 * 
		 * @public
		 * @override
		 */
		destroy : function() {
			UIComponent.prototype.destroy.apply(this, arguments);
		}

	});
	
});