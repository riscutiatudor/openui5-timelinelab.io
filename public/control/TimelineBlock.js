sap.ui.define([
	"sap/ui/core/Control",
	"./TimelineBlockRenderer"
], function(Control, TimelineBlockRenderer) {
	"use strict";

	var oTimelineBlock = Control.extend("ro.riscutiatudor.control.TimelineBlock", {
		metadata: {
			properties: {
				width: {
					type: "sap.ui.core.CSSSize",
					defaultValue: "100%"
				},
				height: {
					type: "sap.ui.core.CSSSize",
					defaultValue: "100%"
				},
				orientation: {
					type: "string"
				},
				title: {
					type: "string"
				},
				text: {
					type: "string"
				}
			}
		},
		
		renderer: TimelineBlockRenderer
	});

	return oTimelineBlock;

});