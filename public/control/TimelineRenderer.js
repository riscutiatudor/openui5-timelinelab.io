sap.ui.define([
	
], function() {
	"use strict";

		var oTimelineRenderer = {};

		oTimelineRenderer.render = function(oRm, oControl) {
			oRm.write("<div");
			oRm.writeControlData(oControl);
			oRm.addClass("timeline");
			oRm.writeClasses();
			oRm.write(">");

			var vOrientation = "right";
			
			// render blocks
			oControl.getBlocks().forEach(function(oBlock) {
				if (vOrientation === "left") {
					vOrientation = "right";
				} else {
					vOrientation = "left";
				}
				
				oBlock.setOrientation(vOrientation);
				
				oRm.renderControl(oBlock);
			});

			oRm.write("</div>");
		};

		return oTimelineRenderer;

	}
);